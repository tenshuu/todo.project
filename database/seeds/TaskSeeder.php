<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 20) as $index) {
            DB::table('tasks')->insert([
                'name' => $faker->sentence(3),
                'creator_id' => $faker->numberBetween($min = 3, $max = 15),
                'isDone' => false,
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime
            ]);
        }

    }
}
