<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class SubtaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1, 30) as $index) {
            DB::table('subtasks')->insert([
                'name' => $faker->sentence(3),
                'description' => $faker->sentence(10),
                'importance' => $faker->numberBetween($min = 1, $max = 5),
                'task_id' => $faker->numberBetween($min = 4, $max = 20),
                'isDone' => $faker->boolean,
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime
            ]);
        }

    }
}
