<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubtasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Создание таблицы
        Schema::create('subtasks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->dateTime('created_at')
                ->default(now());
            $table->dateTime('updated_at')
                ->default(now());
            $table->boolean('isDone')
                ->default(false);
            $table->integer('importance');
            $table->integer('task_id');

            //Каскадное удаление
            $table->foreign('task_id')
                ->references('id')->on('tasks')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subtasks');
    }
}
