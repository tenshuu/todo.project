<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'auth'],
    function () {
        Route::post('login', 'AuthController@login');
        Route::post('registration', 'AuthController@register');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });

Route::prefix('v1')->group(function () {
    Route::prefix('todo')->group(function () {
        //task's routes
        Route::get('/{user}/tasks', 'TasksController@index'); // Show all tasks of user
        Route::get('/{user}/tasks/{task}', 'TasksController@getOne'); // Show one task
        Route::post('/{user}/tasks', 'TasksController@store'); // Add new task
        Route::delete('/{user}/tasks/delete/{task}', 'TasksController@delete'); // Delete the task
        Route::post('/{user}/tasks/update/{task}', 'TasksController@update'); // Update the task
        Route::post('/{user}/tasks/updatestatus/{task}', 'TasksController@updateStatus'); // Update status of the task

        //subtask's routes
        Route::get('/{user}/tasks/subtasks/{subtask}', 'SubtasksController@getOne'); // Show one subtask
        Route::post('/{user}/tasks/{task}/subtasks', 'SubtasksController@store'); // Add new subtask
        Route::delete('/{user}/tasks/subtasks/delete/{subtask}', 'SubtasksController@delete'); // Delete the subtask
        Route::post('/{user}/tasks/subtasks/update/{subtask}', 'SubtasksController@update'); // Update the subtask
        Route::post('/{user}/tasks/subtasks/updatestatus/{subtask}', 'SubtasksController@updateStatus'); // Update status of the subtask
    });
});
