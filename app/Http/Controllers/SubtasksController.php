<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subtasks;
use App\Models\User;
use App\Models\Tasks;

class SubtasksController extends ApiController
{
    #TODO Раскомментировать и исправить
    public function getOne(User $user, Subtasks $subtask)
    {
        $subtask = Subtasks::query()
            ->where('id', '=', $subtask->id)
            ->get();

        return $this->sendResponse($subtask, 'OK', 200);
    }

    public function store(Request $request, User $user, Tasks $task)
    {
        $subtask = new Subtasks();
        return $subtask->createSubtask($request, $user, $task);
    }

    public function delete(User $user, Subtasks $subtask)
    {
        $subtask->deleteSubtask($user, $subtask);
        return $this->sendResponse($subtask, 'OK', 200);
    }

    public function update(Request $request, User $user, Subtasks $subtask)
    {
        $subtask->updateSubtask($request, $user, $subtask);
        return $this->sendResponse($subtask, 'OK', 200);
    }

    #TODO проверка
    public function updateStatus(Request $request, User $user, Subtasks $subtask)
    {
        $subtask->updateSubtaskStatus($request, $user, $subtask);
        return $this->sendResponse($subtask, 'OK', 200);
    }
}
