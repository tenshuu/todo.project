<?php

namespace App\Http\Controllers;

use App\Models\Tasks;
use Illuminate\Http\Request;
use App\Models\TaskFilter;
use App\Models\User;

class TasksController extends ApiController
{
    #TODO пагинация
    public function index(Request $request, User $user)
    {
        $task = Tasks::query()
            ->where('creator_id', '=', $user->id)
            ->orderBy('name')
            ->with('subtasks');

        $task = (new TaskFilter($task, $request))
            ->apply()
            ->get();

        return $this->sendResponse($task, 'OK', 200);
    }

    #TODO Раскомментировать и исправить
    public function getOne(User $user, Tasks $task)
    {
        $task = Tasks::query()
            ->where('id', '=', $task->id)
            ->get();

        return $this->sendResponse($task, 'OK', 200);
    }

    public function store(Request $request, User $user)
    {
        $task = new Tasks();
        return $task->createTask($request, $user);
    }

    public function delete(User $user, Tasks $task)
    {
        $task->deleteTask($user, $task);
        return $this->sendResponse($task, 'OK', 200);
    }

    public function update(Request $request, User $user, Tasks $task)
    {
        $task->updateTask($request, $user, $task);
        return $this->sendResponse($task, 'OK', 200);
    }

    #TODO проверка
    public function updateStatus(Request $request, User $user, Tasks $task)
    {
        $task->updateTaskStatus($request, $user, $task);
        return $this->sendResponse($task, 'OK', 200);
    }
}
