<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $login
 * @property dateTime $updated_at
 * @property dateTime $created_at
 * @property string $password
 * @property string $remember_token
 * @mixin \Illuminate\Contracts\Database\Eloquent\
 */

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [
        'name',
        'login',
        'password'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rules()
    {

        return [
            'name' => 'required|min:4|max:16|without_spaces',
            'login' => 'required|min:4|max:16|without_spaces|unique:users',
            'password' => 'required|min:6|max:16|without_spaces',
        ];
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
