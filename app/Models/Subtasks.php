<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

/**
 * Class Subtasks
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property boolean $isDone
 * @property dateTime $updated_at
 * @property dateTime $created_at
 * @property int $task_id
 * @property int $importance
 * @mixin \Illuminate\Contracts\Database\Eloquent\
 */
class Subtasks extends Model
{
    protected $fillable = [
        'name',
        'description',
        'isDone',
        'updated_at',
        'created_at',
        'task_id',
        'importance'];

    public function rules()
    {
        return [
            'name' => 'required|min:1|max:255',
            'description' => 'max:2000',
            'importance' => 'min:1|max:5',
        ];
    }

    #TODO проверка пользователя
    public function createSubtask(Request $request, User $user, Tasks $task)
    {
        $subtask = new Subtasks();

        $subtask->name = $request->input('name');
        $subtask->description = $request->input('description');
        $subtask->importance = $request->input('importance');
        $subtask->task_id = $task->id;

        $validator = Validator::make($request->all(), $this->rules());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $subtask->save();
            return response()->json('OK', 200);
        }
    }

    #TODO проверка пользователя
    public function deleteSubtask(User $user, Subtasks $subtask)
    {
        return DB::table('subtasks')
            ->where('id', '=', $subtask->id)
            ->delete();
    }

    #TODO проверка пользователя
    public function updateSubtask(Request $request, User $user, Subtasks $subtask)
    {
        if ($request->input('name') != null) {
            $subtask->name = $request->input('name');
        }
        if ($request->input('description') != null) {
            $subtask->description = $request->input('description');
        }
        if ($request->input('nimportance') != null) {
            $subtask->importance = $request->input('importance');
        }
        $subtask->updated_at = now();

        $validator = Validator::make($request->all(), $this->rules());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $subtask->save();
            return response()->json('OK', 200);
        }
    }

    #TODO проверка пользователя
    public function updateSubtaskStatus(Request $request, User $user, Subtasks $subtask)
    {
        if ($request->input('status') != null) {
            $subtask->isDone = $request->input('status');
            $subtask->updated_at = now();

            $subtask->save();
        }
    }
}
