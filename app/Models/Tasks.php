<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

/**
 * Class Tasks
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property boolean $isDone
 * @property dateTime $updated_at
 * @property dateTime $created_at
 * @property int $creator_id
 * @mixin \Illuminate\Contracts\Database\Eloquent\
 */
class Tasks extends Model
{
    protected $fillable = [
        'name',
        'creator_id',
        'isDone',
        'updated_at',
        'created_at'];

    public function rules()
    {
        return [
            'name' => 'required|min:1|max:255',
        ];
    }

    public function subtasks()
    {
        return $this->hasMany(Subtasks::class, 'task_id');
    }


    #TODO проверка пользователя
    public function createTask(Request $request, User $user)
    {
        $task = new Tasks();

        $task->creator_id = $user->id;
        $task->name = $request->input('name');

        $validator = Validator::make($request->all(), $this->rules());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $task->save();
            return response()->json('OK', 200);
        }
    }

    #TODO проверка пользователя
    public function deleteTask(User $user, Tasks $task)
    {
        return DB::table('tasks')
            ->where('id', '=', $task->id)
            ->delete();
    }

    #TODO проверка пользователя
    public function updateTask(Request $request, User $user, Tasks $task)
    {
        $task->name = $request->input('name');
        $task->updated_at = now();

        $validator = Validator::make($request->all(), $this->rules());

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        } else {
            $task->save();
            return response()->json('OK', 200);
        }
    }

    #TODO проверка пользователя
    public function updateTaskStatus(Request $request, User $user, Tasks $task)
    {
        if ($request->input('status') != null) {
        $task->isDone = $request->input('status');
        $task->updated_at = now();

        $task->save();
        }
    }
}
