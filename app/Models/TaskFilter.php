<?php

namespace App\Models;

class TaskFilter
{

    protected $builder;
    protected $request;

    public function __construct($builder, $request)
    {

        $this->builder = $builder;
        $this->request = $request;
    }

    public function apply()
    {
        foreach ($this->filters() as $filter => $value) {
            if (method_exists($this, $filter)) {
                $this->$filter($value);
            }
        }
        return $this->builder;
    }

    public function status($value)
    {
        $this->builder->where('status', '=', $value);
    }

    public function filters()
    {
        return $this->request->all();
    }
}
